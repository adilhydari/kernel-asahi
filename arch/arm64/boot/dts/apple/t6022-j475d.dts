// SPDX-License-Identifier: GPL-2.0+ OR MIT
/*
 * Mac Studio (M2 Ultra, 2023)
 *
 * target-type: J475d
 *
 * Copyright The Asahi Linux Contributors
 */

/dts-v1/;

#define NO_DCP

#include "t6022.dtsi"
#include "t602x-j474-j475.dtsi"
#include "t6022-jxxxd.dtsi"

/ {
	compatible = "apple,j475d", "apple,t6022", "apple,arm-platform";
	model = "Apple Mac Studio (M2 Ultra, 2023)";
	aliases {
		atcphy4 = &atcphy0_die1;
		atcphy5 = &atcphy1_die1;
	};
};

&framebuffer0 {
	power-domains = <&ps_dispext0_cpu0_die1>, <&ps_dptx_phy_ps_die1>;
};

&typec4 {
	label = "USB-C Front Right";
};

&typec5 {
	label = "USB-C Front Left";
};

/* delete unused USB nodes on die 1 */

/delete-node/ &dwc3_2_dart_0_die1;
/delete-node/ &dwc3_2_dart_1_die1;
/delete-node/ &dwc3_2_die1;
/delete-node/ &atcphy2_die1;

/delete-node/ &dwc3_3_dart_0_die1;
/delete-node/ &dwc3_3_dart_1_die1;
/delete-node/ &dwc3_3_die1;
/delete-node/ &atcphy3_die1;


/* delete unused always-on power-domains on die 1 */

/delete-node/ &ps_atc2_usb_aon_die1;
/delete-node/ &ps_atc2_usb_die1;

/delete-node/ &ps_atc3_usb_aon_die1;
/delete-node/ &ps_atc3_usb_die1;

&wifi0 {
	compatible = "pci14e4,4434";
	brcm,board-type = "apple,canary";
};

&bluetooth0 {
	compatible = "pci14e4,5f72";
	brcm,board-type = "apple,canary";
};

&sound {
	model = "Mac Studio J475";
};
